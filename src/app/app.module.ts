import {Http, HttpModule, RequestOptions} from '@angular/http';
import {AuthConfig, AuthHttp} from 'angular2-jwt';
import {AppComponent} from './app.component';
import {MenuComponent} from './menu/menu.component';
import {LoginComponent} from './login/login.component';
import {HeaderComponent} from './header/header.component';
import {CrmPageComponent} from './pages/crm-page/crm-page.component';
import {MenuLinkComponent} from './menu/menu-link/menu-link.component';
import {CrmListPageComponent} from './pages/crm-list-page/crm-list-page.component';
import {SettingsPageComponent} from './pages/settings-page/settings-page.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {DashboardPageComponent} from './pages/dashboard-page/dashboard-page.component';
import {CrmDetailsPageComponent} from './pages/crm-details-page/crm-details-page.component';
import {IndicatorsPageComponent} from './pages/indicators-page/indicators-page.component';
import {CrmContactsPageComponent} from './pages/crm-contacts-page/crm-contacts-page.component';
import {DialogAddContactComponent} from './pages/customer-contact/dialog-add-contact/dialog-add-contact.component';
import {QuotationPrincipalComponent} from './pages/business-management-quotation-details-page/widgets/quotation-principal/quotation-principal.component';
import {ProjectMonitoringPageComponent} from './pages/project-monitoring-page/project-monitoring-page.component';
import {BusinessManagementPageComponent} from './pages/business-management-page/business-management-page.component';
import {GeneralInformationWidgetComponent} from './pages/business-management-quotation-details-page/widgets/general-information-widget/general-information-widget.component';
import {BusinessManagementBusinessListPageComponent} from './pages/business-management-business-list-page/business-management-business-list-page.component';
import {BusinessManagementQuotationListPageComponent} from './pages/business-management-quotation-list-page/business-management-quotation-list-page.component';
import {BusinessManagementBusinessDetailsPageComponent} from './pages/business-management-business-details-page/business-management-business-details-page.component';
import {BusinessManagementQuotationDetailsPageComponent} from './pages/business-management-quotation-details-page/business-management-quotation-details-page.component';
import {Routing} from './app.routing';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {MatDialogModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthGuard} from './_guard/auth.guard';
import {LocalStorageService} from './services/local-storage.service';
import {AccessControlService} from './services/access-control.service';
import {CgvRepositoryService} from './repository/cgv-repository.service';
import {EtatRepositoryService} from './repository/etat-repository.service';
import {NotificationsService} from './services/notifications.service';
import {AuthenticationService} from './services/authentication.service';
import {FunctionContactService} from './repository/function-contact.service';
import {UserUtilRepositoryService} from './repository/user-util-repository.service';
import {CustomerRepositoryService} from './repository/customer-repository.service';
import {BusinessRepositoryService} from './repository/business-repository.service';
import {QuotationRepositoryService} from './repository/quotation-repository.service';
import {TypeBenefitRepositoryService} from './repository/type-benefit-repository.service';
import {CustomerContactRepositoryService} from './repository/customer-contact-repository.service';
import {PaymentConditionRepositoryService} from './repository/payment-condition-repository.service';
import {NgModule} from '@angular/core';
import {AdministationPageComponent} from './pages/administation-page/administation-page.component';
import {UserListPageComponent} from './pages/user-list-page/user-list-page.component';
import {UserDetailsPageComponent} from './pages/user-details-page/user-details-page.component';
import {UserAccessPageComponent} from './pages/user-access-page/user-access-page.component';
import {RolesRepositoryService} from './repository/roles-repository.service';
import {ModuleRepositoryService} from './repository/module-repository.service';
import {UnitRepositoryService} from './repository/unit-repository.service';
import {LineComponent} from './pages/business-management-quotation-details-page/widgets/quotation-principal/line/line.component';
import {ReferentialComponent} from './pages/settings-page/referential/referential.component';
import {DialodAddRoleComponent} from './pages/user-access-page/dialod-add-role/dialod-add-role.component';
import {QuotationLineRepositoryService} from './repository/quotation-line-repository.service';
import {GridComponent} from './grid/grid.component';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
    return new AuthHttp(new AuthConfig({}), http, options);
}

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        LineComponent,
        LoginComponent,
        HeaderComponent,
        CrmPageComponent,
        MenuLinkComponent,
        CrmListPageComponent,
        ReferentialComponent,
        SettingsPageComponent,
        UserListPageComponent,
        DialodAddRoleComponent,
        NotificationsComponent,
        DashboardPageComponent,
        CrmDetailsPageComponent,
        IndicatorsPageComponent,
        UserAccessPageComponent,
        CrmContactsPageComponent,
        UserDetailsPageComponent,
        DialogAddContactComponent,
        AdministationPageComponent,
        QuotationPrincipalComponent,
        ProjectMonitoringPageComponent,
        BusinessManagementPageComponent,
        GeneralInformationWidgetComponent,
        BusinessManagementBusinessListPageComponent,
        BusinessManagementQuotationListPageComponent,
        BusinessManagementBusinessDetailsPageComponent,
        BusinessManagementQuotationDetailsPageComponent,
        GridComponent,
    ],
    imports: [
        Routing,
        HttpModule,
        FormsModule,
        BrowserModule,
        MatDialogModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
    ],
    entryComponents: [DialogAddContactComponent, DialodAddRoleComponent],
    providers: [
        AuthGuard,
        FormBuilder,
        LocalStorageService,
        AccessControlService,
        NotificationsService,
        CgvRepositoryService,
        EtatRepositoryService,
        AuthenticationService,
        UnitRepositoryService,
        FunctionContactService,
        RolesRepositoryService,
        ModuleRepositoryService,
        UserUtilRepositoryService,
        CustomerRepositoryService,
        BusinessRepositoryService,
        QuotationRepositoryService,
        TypeBenefitRepositoryService,
        QuotationLineRepositoryService,
        CustomerContactRepositoryService,
        PaymentConditionRepositoryService,
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions]
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
