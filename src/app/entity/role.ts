import {BaseEntity} from './base-entity';
import {Module} from './module';
import {isNull, isNullOrUndefined} from 'util';

export class Role extends BaseEntity {

    id;
    title;
    modules: Module[];

    constructor(data: any = {}) {
        super(data);

        this.id = data.id;
        this.title = data.title;
        this.modules = [];

        if (!isNullOrUndefined(data.modules)) {
            this.modules = data.modules.map(moduleAccess => {
                return new Module(moduleAccess);
            });
        }
    }

    /**
     * @param {Module} module
     * @returns {this}
     */
    addModule(module: Module) {
        this.modules.push(module);

        return this;
    }

    /**
     * @param {string} moduleTitle
     * @returns {this}
     */
    deleteModule(moduleTitle: string) {

        this.modules.forEach((mod, key) => {
            if (mod.title === moduleTitle) {
                this.modules.splice(key, 1);
            }
        });

        return this;
    }

    /**
     * @param {string} moduleTitle
     * @returns {boolean}
     */
    isAllowedToModule(moduleTitle: string): boolean {
        return !isNull(this.findModule(moduleTitle));
    }

    /**
     * @param {string} moduleTitle
     * @returns Module
     */
    findModule(moduleTitle: string): boolean {
        let moduleFound = null;

        this.modules.forEach(module => {
            if (module.title === moduleTitle) {
                moduleFound = module;
            }
        });

        return moduleFound;
    }
}
