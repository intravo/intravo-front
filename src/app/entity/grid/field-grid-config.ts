import {isNull} from 'util';

export class FieldGridConfig {

    public label: string;
    public fieldName: string;
    public isFilterable: boolean;
    public customRender;

    constructor(label, fieldName, isFilterable = false, customRender = null) {
        this.label = label;
        this.fieldName = fieldName;
        this.isFilterable = isFilterable;
        this.customRender = customRender;
    }

    render(fieldValue) {
        if (!isNull(this.customRender)) {
            return this.customRender(fieldValue);
        } else {
            return fieldValue;
        }
    }
}
