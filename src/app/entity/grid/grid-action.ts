export class GridAction {

    public icon: string;
    public callable: any;

    constructor(icon, callable) {
        this.icon = icon;
        this.callable = callable;
    }

    action(entity) {
        return this.callable(entity);
    }
}
