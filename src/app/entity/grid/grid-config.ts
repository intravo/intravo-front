import {GridAction} from './grid-action';

export class GridConfig {

    public fields;
    public detailsRoute: string;
    public detailsAction: boolean;
    public removeAction: boolean;
    public customActions: GridAction[];

    constructor(fields) {
        this.fields = fields;
        this.customActions = [];
        this.removeAction = true;
        this.detailsAction = true;
    }

    addCustomAction(icon: string, callable: any = null) {
        this.customActions.push(new GridAction(icon, callable));
    }
}
