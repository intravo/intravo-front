import {BaseEntity} from './base-entity';
import {UserUtil} from './user-util';
import {Customer} from './customer';
import {isUndefined} from 'util';
import {Contact} from './contact';
import {Quotation} from './quotation';

export class Business extends BaseEntity {

    public id: number;
    public code: string;
    public libelle: string;
    public note: string;
    public file: string;
    public user_com: UserUtil;
    public user_tec: UserUtil;
    public customer: Customer;
    public contact_com: Contact;
    public contact_tec: Contact;
    public quotation: Quotation[];

    constructor(data: any) {

        super(data);

        this.id = data['id'];
        this.code = data['code'];
        this.libelle = data['libelle'];
        this.note = data['note'];
        this.file = data['file'];
        this.user_com = data['user_com'];
        this.user_tec = data['user_tec'];
        this.customer = data['customer'];

        this.contact_com = new Contact();

        if (!isUndefined(data['comContact'])) {
            this.contact_com = new Contact(data['comContact']);
        }

        this.contact_tec = new Contact();

        if (!isUndefined(data['techContact'])) {
            this.contact_tec = new Contact(data['techContact']);
        }

        this.quotation = [];

        if (data['quotation']) {
            this.quotation = data['quotation'].map(item => new Quotation(item));
        }

        return this;
    }
}
