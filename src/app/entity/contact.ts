import {Fonction} from './contact-fonction';
import {BaseEntity} from './base-entity';
import {isUndefined} from 'util';
import {Customer} from './customer';

export class Contact extends BaseEntity {

    public id: any;
    public firstName: any;
    public lastName: any;
    public phone: any;
    public mobilePhone?: any;
    public email: any;
    public customer: Customer;
    public fonction: Fonction;

    constructor(data: any = {}) {

        super(data);

        this.id = data['id'];
        this.firstName = data['firstName'];
        this.lastName = data['lastName'];
        this.phone = data['phone'];
        this.mobilePhone = data['mobilePhone'];
        this.email = data['email'];

        if (!isUndefined(data['function'])) {
            this.fonction = new Fonction(data['function']);
        }

        if (!isUndefined(data['customer'])) {
            this.customer = new Customer(data['customer']);
        }

        return this;
    }
}