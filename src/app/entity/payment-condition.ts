import {BaseEntity} from './base-entity';

export class PaymentCondition extends BaseEntity {

    public id: number;
    public title: string;

    /**
     * Set datas
     *
     * @param data
     */
    constructor(data: any) {

        super(data);

        this.id = data['id'];
        this.title = data['title'];
    }
}