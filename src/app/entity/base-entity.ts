export abstract class BaseEntity {

    constructor(data: any = {}) {
        this['@id'] = typeof data['@id'] !== 'undefined' ? data['@id'] : undefined;
        this['@type'] = typeof data['@type'] !== 'undefined' ? data['@type'] : undefined;
    }

    /**
     * setData()
     *
     * @param data
     * @returns {BaseEntity}
     */
    public setData(data: any): this {
        this.constructor(data);

        return this;
    }

}
