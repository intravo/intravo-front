import {BaseEntity} from './base-entity';
import {Unit} from './unit';
import {isNullOrUndefined} from 'util';

export class QuotationLine extends BaseEntity {

    public id: number;
    public designation: string;
    public quantity: number;
    public priceHT: number;
    public position: number;
    public costHT: number;
    public isOptional: boolean;
    public isGift: boolean;
    public isTitle: boolean;
    public unit: Unit;

    /**
     * constructor()
     *
     * @param data
     */
    constructor(data: any = {}) {

        super(data);

        this.id = data['id'];
        this.designation = data['designation'] ? data['designation'] : '';
        this.quantity = parseInt(data['quantity'], 10) ? parseInt(data['quantity'], 10) : 0;
        this.priceHT = parseFloat(data['priceHT']) ? parseFloat(data['priceHT']) : 0;
        this.costHT = 0;
        this.position = data['position'];
        this.isOptional = !isNullOrUndefined(data['isOptional']) ? data['isOptional'] : false;
        this.isGift = data['isGift'];
        this.isTitle = data['isTitle'];
        this.unit = !isNullOrUndefined(data['unit']) ? new Unit(data['unit']) : new Unit();

        this.calculatePrices();
    }

    calculatePrices() {
        this.costHT = this.quantity * this.priceHT;
    }
}
