import {BaseEntity} from './base-entity';

export class Module extends BaseEntity {

    id: number;
    title: string;

    constructor(data: any = {}) {
        super(data);

        this.id = data.id;
        this.title = data.title;
    }

    /**
     * @returns {}
     */
    static getMappingLabels() {
        return {
            'business-management': 'Affaires',
            'dashboard': 'Dashboard',
            'administration': 'Administration',
            'indicators': 'Indicateurs',
            'project-monitoring': 'Suivi de projet',
            'crm': 'CRM',
        };
    }

    /**
     * @returns {string}
     */
    getLabel(): string {
        return Module.getMappingLabels()[this.title];
    }
}
