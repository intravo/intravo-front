import {BaseEntity} from './base-entity';

export class Fonction extends BaseEntity {

    public id: number;
    public title: string;

    constructor(data: any = {}) {

        super(data);

        this.id = data['fonctionId'];
        this.title = data['title'];
    }
}