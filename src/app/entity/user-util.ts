import {BaseEntity} from './base-entity';
import {Role} from './role';
import {isNullOrUndefined} from 'util';

export class UserUtil extends BaseEntity {

    public id: number;
    public email: string;
    public fullname: string;
    public username: string;
    public roles: any;
    public role: Role;
    public fistName: string;
    public lastName: string;
    public plainPassword: string;

    /**
     * setData(data)
     *
     * @param data
     * @returns {UserUtil}
     */
    constructor(data: any = {}) {
        super(data);

        this.id = data.id;
        this.email = data.email;
        this.fullname = data.fullname;
        this.username = data.username;
        const explodeFullName = !isNullOrUndefined(this.fullname) ? this.fullname.split(' ') : '';
        this.fistName = (explodeFullName[0]) ? explodeFullName[0] : 'N/A';
        this.lastName = (explodeFullName[1]) ? explodeFullName[1] : 'N/A';
        this.role = new Role();

        if (!isNullOrUndefined(data['role'])) {
            this.role = new Role(data['role']);
        }

        this.plainPassword = '';
    }

    /**
     * @param {string} moduleTitle
     * @returns {boolean}
     */
    isAllowedToModule(moduleTitle: string): boolean {
        if (moduleTitle === '*') {
            return true;
        }

        return this.role.isAllowedToModule(moduleTitle);
    }
}
