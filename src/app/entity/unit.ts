import {Referential} from './Referential';

export class Unit extends Referential {

    constructor(data: any = {}) {
        super(data);

        return this;
    }
}
