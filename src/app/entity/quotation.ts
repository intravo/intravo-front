import {BaseEntity} from './base-entity';
import {Etat} from './etat';
import {PaymentCondition} from './payment-condition';
import {Cgv} from './cgv';
import {TypeBenefit} from './type-benefit';
import {Business} from './business';
import {Customer} from './customer';
import {QuotationLine} from './quotation-line';
import {isNullOrUndefined} from 'util';

export class Quotation extends BaseEntity {

    public id: number;
    public code: string;
    public title: string;
    public creation: string;
    public validity: string;
    public customerNote: string;
    public internalNote: string;
    public totalPriceHt: number;
    public discountTotalPrice: number;
    public totalPriceTva: number;
    public totalPriceTtc: number;
    public discountHt: number;
    public tva: number;
    public etat: any;
    public paymentCondition: any;
    public cgv: any;
    public benefit: any;
    public business: any;
    public customer: Customer;
    public quotationLines: QuotationLine[];

    /**
     * @param data
     */
    constructor(data: any = {}) {
        super(data);

        this.id = data['id'];
        this.code = data['code'];
        this.title = data['title'];
        this.creation = data['creation'];
        this.validity = data['validity'];
        this.customerNote = data['customerNote'];
        this.internalNote = data['internalNote'];
        this.totalPriceHt = data['totalPriceHt'] ? parseFloat(data['totalPriceHt']) : 0;
        this.discountTotalPrice = data['discountTotalPrice'] ? parseFloat(data['discountTotalPrice']) : 0;
        this.totalPriceTva = data['totalPriceTva'] ? parseFloat(data['totalPriceTva']) : 0;
        this.totalPriceTtc = data['totalPriceTtc'] ? parseFloat(data['totalPriceTtc']) : 0;
        this.etat = typeof data['etat'] !== 'undefined' ? new Etat(data['etat']) : null;
        this.paymentCondition = typeof data['paymentCondition'] !== 'undefined' ? new PaymentCondition(data['paymentCondition']) : null;
        this.cgv = typeof data['cgv'] !== 'undefined' ? new Cgv(data['cgv']) : null;
        this.benefit = typeof data['benefit'] !== 'undefined' ? new TypeBenefit(data['benefit']) : null;
        this.business = typeof data['business'] !== 'undefined' ? new Business(data['business']) : new Business({});
        this.customer = typeof data['customer'] !== 'undefined' ? new Customer(data['customer']) : new Customer({});
        this.quotationLines = [];

        if (!isNullOrUndefined(data['quotationLines'])) {
            this.quotationLines = data['quotationLines'].map(quotationLines => new QuotationLine(quotationLines));
            this.sortLines();
        }

        this.calculatePrices();
    }

    /**
     * @param {QuotationLine} quotationLine
     */
    addLine(quotationLine: QuotationLine) {
        quotationLine.position = this.quotationLines.length;
        this.quotationLines.push(quotationLine);
    }

    /**
     * @param {QuotationLine} quotationLine
     */
    removeLine(quotationLine: QuotationLine) {
        this.quotationLines = this.quotationLines.filter(q => quotationLine !== q);
    }

    /**
     * @param {QuotationLine} quotationLine
     * @returns {any}
     */
    getPosition(quotationLine: QuotationLine) {
        let position = null;

        this.quotationLines.forEach((item, key) => {
            if (quotationLine === item) {
                position = key;
                return;
            }
        });

        return position;
    }

    /**
     * @param {QuotationLine} quotationLine
     */
    upLine(quotationLine: QuotationLine) {
        const position = this.getPosition(quotationLine);

        if (position > 0 && this.quotationLines.length >= 2) {
            this.quotationLines[position - 1].position = quotationLine.position--;
        }

        this.sortLines();
    }

    /**
     * @param {QuotationLine} quotationLine
     */
    downLine(quotationLine: QuotationLine) {
        const position = this.getPosition(quotationLine);

        if (position < this.quotationLines.length - 1 && this.quotationLines.length >= 2) {
            this.quotationLines[position + 1].position = quotationLine.position++;
        }

        this.sortLines();
    }

    /**
     * Re-order lines
     */
    sortLines() {
        this.quotationLines.sort(function (a, b) {
            return a.position - b.position;
        });

        this.quotationLines.forEach((item, key) => {
            item.position = key;
        });
    }

    /**
     * Calculate all prices
     */
    calculatePrices() {
        this.totalPriceHt = 0;
        this.totalPriceTva = 0;
        this.discountTotalPrice = 0;
        this.discountHt = 0;

        this.quotationLines.forEach(line => {
            if (!line.isTitle) {

                this.totalPriceHt += line.priceHT * line.quantity;

                if (line.isGift) {
                    this.discountHt += line.priceHT * line.quantity;
                }
            }
        });

        this.discountTotalPrice = this.totalPriceHt - this.discountHt;
        this.tva = this.discountTotalPrice * 0.2;
        this.totalPriceTtc = this.discountTotalPrice + this.tva;
    }
}
