import {BaseEntity} from './base-entity';
import {FormBuilder} from '@angular/forms';

export class Referential extends BaseEntity {
    public id: Number;
    public title: string;

    constructor(data: any = {}) {
        super(data);

        this.id = data['id'];
        this.title = data['title'];

        return this;
    }

    buildForm(fb: FormBuilder) {
        return fb.group({
            id: [this.id],
            title: [this.title]
        });
    }
}
