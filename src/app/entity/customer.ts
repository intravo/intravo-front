import {BaseEntity} from './base-entity';
import {CustomerContact} from './customer-contact';
import {Business} from './business';
import {Quotation} from './quotation';
import {isNullOrUndefined} from 'util';

export class Customer extends BaseEntity {
    id: number;
    code: string;
    name: string;
    socialreason: string;
    siret: string;
    address: string;
    addressComplement: string;
    postalCode: string;
    city: string;
    phone: string;
    mobile: string;
    memo: string;
    contact: CustomerContact[];
    business: Business[];
    quotation: Quotation[];

    constructor(data: any = {}) {

        super(data);

        this.id = data['id'];
        this.code = data['code'];
        this.name = data['name'];
        this.socialreason = data['socialReason'];
        this.siret = data['siret'];
        this.address = data['address'];
        this.addressComplement = data['addressComplement'];
        this.postalCode = data['postalCode'];
        this.city = data['city'];
        this.phone = data['phone'];
        this.mobile = data['mobile'];
        this.memo = data['memo'];

        this.contact = [];

        if (!isNullOrUndefined(data['contact'])) {
            this.contact = data['contact'].map(item => new CustomerContact(item));
        }

        this.business = [];

        if (!isNullOrUndefined(data['business'])) {
            this.business = data['business'].map(item => new Business(item));
        }

        this.quotation = [];

        if (!isNullOrUndefined(data['quotation'])) {
            this.quotation = data['quotation'].map(item => new Quotation(item));
        }
    }
}
