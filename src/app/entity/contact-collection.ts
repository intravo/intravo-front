import {CustomerContact} from './customer-contact';

export class ContactCollection {

    contacts: CustomerContact[];

    constructor(contacts: any) {
        this.contacts = contacts.map(item => new CustomerContact(item));
    }

    /**
     * @returns {CustomerContact[]}
     */
    getContacts() {
        return this.contacts;
    }
}
