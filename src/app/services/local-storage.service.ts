import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

    /**
     * set()
     *
     * @param key
     * @param content
     */
    public set(key: string, content: any) {

        let settedContent: any;

        switch (typeof content) {
            case 'string':
                settedContent = content;
                break;
            default:
                settedContent = JSON.stringify(content);
                break;
        }

        localStorage.setItem(key, settedContent);
    }

    /**
     * get()
     *
     * @param key
     * @returns {string|null}
     */
    public get(key: string) {
        return localStorage.getItem(key);
    }

}
