import {EventEmitter, Injectable} from '@angular/core';
import {Notification} from '../entity/notification';

@Injectable()
export class NotificationsService {

    private notificationPush: EventEmitter<Notification>;

    constructor() {
        this.notificationPush = new EventEmitter<Notification>();
    }

    notificationPushEvent() {
        return this.notificationPush;
    }

    push(type, message) {
        const notification = new Notification();
        notification.type = type;
        notification.message = message;
        notification.visible = true;

        this.notificationPush.emit(notification);
    }

    success(message) {
        this.push('success', message);
    }

    error(message) {
        this.push('error', message);
    }

    warning(message) {
        this.push('warning', message);
    }
}
