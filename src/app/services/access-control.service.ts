import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class AccessControlService {

    private roleList: Array<String> = new Array<String>();

    constructor(private router: Router) {
    }

    /**
     * addRoles()
     *
     * @param roles
     */
    public addRoles(roles: Array<String>) {
        for (let role of roles) {
            this.roleList.push(role);
        }
    }

    /**
     * checkPermission()
     *
     * @param role
     */
    public checkPermission(role: string) {

        for (let role of this.roleList) {
            if (role === role) {
                return true;
            }
        }

        this.router.navigate(['']);

    }

}
