import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {environment} from '../../environments/environment';
import {tokenNotExpired} from 'angular2-jwt';
import {UserUtilRepositoryService} from '../repository/user-util-repository.service';
import {UserUtil} from '../entity/user-util';

@Injectable()
export class AuthenticationService {

    static readonly LOGIN_CODE_SUCCESS = 0;
    static readonly LOGIN_CODE_BAD_CREDENTIALS = 1;
    static readonly LOGIN_CODE_INTERNAL_ERROR = 2;

    private url: string = environment.loginURL;
    private loggedInUser: UserUtil;
    private loginEvent: EventEmitter<AuthenticationService>;
    private codeLogin: EventEmitter<number>;
    private isInit: boolean;

    /**
     * @param http
     * @param userUtilRepository
     */
    constructor(private http: HttpClient,
                private userUtilRepository: UserUtilRepositoryService) {
        this.loggedInUser = null;
        this.loginEvent = new EventEmitter<this>();
        this.codeLogin = new EventEmitter<number>();
        this.retrieveConnexion();
    }

    /**
     * @returns {boolean}
     */
    isInitialized(): boolean {
        return this.isInit;
    }

    /**
     * Retrieve the initial connexion if the user refresh the page
     */
    retrieveConnexion(): void {
        if (tokenNotExpired('id_token')) {

            const userId = localStorage.getItem('user_id');

            this.userUtilRepository
                .get({id: userId})
                .subscribe(userData => {
                    const user = this.convertDataUserToUser(userData);

                    if (user !== null) {
                        this.loggedInUser = user;
                        this.loginEvent.emit(this);
                    }
                    this.isInit = true;
                });
        } else {
            this.isInit = true;
        }
    }

    /**
     * @returns {EventEmitter<AuthenticationService>}
     */
    onLoginChanges() {
        return this.loginEvent;
    }

    /**
     * @returns {EventEmitter<number>}
     */
    onStateLoginChanges() {
        return this.codeLogin;
    }

    /**
     * @param {string} username
     * @param {string} password
     * @returns {EventEmitter<boolean>}
     */
    authenticate(username: string, password: string) {
        const headers = {headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})};
        const body = '_username=' + username + '&_password=' + password;

        this.http.post(this.url, body, headers).subscribe(
            tokenData => {
                this.userUtilRepository.get({username: username}).subscribe(
                    userData => {
                        if (this.login(tokenData, userData)) {
                            this.codeLogin.emit(AuthenticationService.LOGIN_CODE_SUCCESS);
                        } else {
                            this.codeLogin.emit(AuthenticationService.LOGIN_CODE_INTERNAL_ERROR);
                        }
                    },
                    () => this.codeLogin.emit(AuthenticationService.LOGIN_CODE_INTERNAL_ERROR)
                );
            },
            error => {
                if (error.status === 401) {
                    this.codeLogin.emit(AuthenticationService.LOGIN_CODE_BAD_CREDENTIALS);
                } else {
                    this.codeLogin.emit(AuthenticationService.LOGIN_CODE_INTERNAL_ERROR);
                }
            }
        );

        return this.codeLogin;
    }

    logout() {
        localStorage.removeItem('id_token');
        localStorage.removeItem('user_id');
        this.loggedInUser = null;
    }

    /**
     *
     * @returns {boolean|UserUtil}
     */
    isLoggedIn() {
        return this.loggedInUser !== null;
    }

    /**
     * @param tokenData
     * @param userData
     * @returns {boolean}
     */
    login(tokenData: any, userData: any) {

        const user = this.convertDataUserToUser(userData);

        if (user !== null) {
            localStorage.setItem('id_token', tokenData['token'].toString());
            localStorage.setItem('user_id', user.id.toString());

            this.loggedInUser = user;
            this.loginEvent.emit(this);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @returns {UserUtil[]}
     */
    getLoggedInUser() {
        return this.loggedInUser;
    }

    /**
     * @param userData
     * @returns {number|BaseEntity|any}
     */
    convertDataUserToUser(userData: any): UserUtil {

        const users = this.userUtilRepository.mapData(userData);

        return typeof users[0] !== 'undefined' ? users[0] : null;
    }
}
