import {Component, OnInit} from '@angular/core';
import {NotificationsService} from '../services/notifications.service';
import {Notification} from '../entity/notification';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

    notifications: Notification[];

    constructor(private notificationService: NotificationsService) {
        this.notifications = [];
    }

    ngOnInit() {
        this.notificationService.notificationPushEvent().subscribe(notification => {
            this.notifications.push(notification);

            setTimeout(() => {
                notification.visible = false;
            }, 5000);
        });
    }
}
