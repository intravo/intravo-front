import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';
import {UserUtil} from '../entity/user-util';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    public connectedUser: UserUtil;

    constructor(private authenticationService: AuthenticationService,
                private router: Router) {
        this.connectedUser = null;
    }

    ngOnInit() {
        this.authenticationService.onLoginChanges().subscribe(auth => {
            this.connectedUser = auth.getLoggedInUser();
        });
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['login']);
    }

}
