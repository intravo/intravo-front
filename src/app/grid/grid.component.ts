import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {NotificationsService} from '../services/notifications.service';
import {Subscription} from 'rxjs/Subscription';
import {BaseEntity} from '../entity/base-entity';
import {BaseRepository} from '../repository/base-repository';
import {GridConfig} from '../entity/grid/grid-config';
import {FieldGridConfig} from '../entity/grid/field-grid-config';

@Component({
    selector: 'app-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, OnChanges {

    @Input() search: string;
    @Input() gridConfig: GridConfig;
    @Input() repository: BaseRepository;
    @Input() list: BaseEntity[];
    private entities: BaseEntity[];
    private subscription: Subscription;
    private page: number;
    private orderField: string;
    private order: string;
    private pages: any;
    private loader: boolean;
    private fields: FieldGridConfig[];

    /**
     * @param {NotificationsService} notificationService
     */
    constructor(private notificationService: NotificationsService) {
        if (!this.list) {
            this.subscription = null;
            this.search = null;
            this.page = 1;
            this.orderField = null;
            this.order = null;
            this.pages = [];
        } else {
            this.entities = this.list;
        }
    }

    /**
     * ngOnInit()
     */
    ngOnInit() {
        this.fields = this.gridConfig.fields;

        if (!this.list) {
            // Retrieve last requested search
            this.repository.retrieveSearch();
            this.search = this.repository.getCollectionSearch();
            this.page = this.repository.getCollectionPage();
            this.orderField = this.repository.getCollectionOrderField();
            this.order = this.repository.getCollectionOrder();

            // Load the list
            this.refresh();
        }
    }

    /**
     * Make the ajax request to load the list
     */
    refresh() {
        if (!this.list) {
            this.loader = true;

            if (this.subscription) {
                this.subscription.unsubscribe();
            }

            this.subscription = this.repository
                .getCollection()
                .subscribe(data => {
                    this.entities = this.repository.mapData(data);
                    this.pages = this.repository.getPages(data);
                    this.page = this.repository.getCollectionPage();
                    this.order = this.repository.getCollectionOrder();
                    this.orderField = this.repository.getCollectionOrderField();
                    this.loader = false;
                });
        }
    }

    /**
     * Search
     * @param {SimpleChanges} changes
     */
    ngOnChanges(changes: SimpleChanges) {
        if (!this.list) {
            if (changes['search']) {
                this.repository.setCollectionSearch(this.search);
                this.refresh();
            }
        } else {
            if (changes['list']) {
                this.entities = this.list;
            }
        }
    }

    /**
     * Change the collection order by
     *
     * @param field
     */
    changeOrder(field) {
        this.repository.changeCollectionOrder(field);
        this.refresh();
    }

    /**
     * @param field
     * @returns {boolean}
     */
    showOrderAscClass(field) {
        return this.orderField === field && this.order === 'asc';
    }

    /**
     * @param field
     * @returns {boolean}
     */
    showOrderDescClass(field) {
        return this.orderField === field && this.order === 'desc';
    }

    /**
     * @param page
     */
    goToPage(page) {
        this.page = page;
        this.repository.setCollectionPage(this.page);
        this.refresh();
    }

    /**
     * @param {number} id
     */
    remove(id: number) {
        if (confirm('Êtes-vous certain ?')) {
            this.loader = true;
            this.repository.remove(id).subscribe(() => {
                this.refresh();
                this.notificationService.success('Entrée supprimée avec succès');
            });
        }
    }
}
