import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authentication: AuthenticationService, private router: Router) {
    }

    /**
     * canActivate()
     *
     * @returns {boolean}
     */
    canActivate(snapshot: ActivatedRouteSnapshot) {

        if (this.authentication.isInitialized()) {
            if (this.authentication.isLoggedIn() === true) {

                return this.authentication.getLoggedInUser().isAllowedToModule(snapshot.data.module);
            } else {
                this.router.navigate(['login']);
                return false;
            }
        } else {
            return true;
        }
    }
}
