import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {FunctionContact} from '../entity/function-contact';

@Injectable()
export class FunctionContactService extends BaseRepository {

    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getFoncContact()
     *
     * @param params
     * @returns {Observable<Object>}
     */
    public getFoncContact(params: any = {}) {
        return this.get(params);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    protected getEntityName(): string {
        return 'contact_functions';
    }

    /**
     * getFilterableFields()
     *
     * @returns {undefined}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {FunctionContact}
     */
    protected createNewEntity(data: any): FunctionContact {
        return new FunctionContact(data);
    }
}
