import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {TypeBenefit} from '../entity/type-benefit';

@Injectable()
export class TypeBenefitRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'type_benefits';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[]}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {TypeBenefit}
     */
    protected createNewEntity(data: any): TypeBenefit {
        return new TypeBenefit(data);
    }
}
