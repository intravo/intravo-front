import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {UserUtil} from '../entity/user-util';

@Injectable()
export class UserUtilRepositoryService extends BaseRepository {

    /**
     * Constructor
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * @returns {string}
     */
    public getEntityName(): string {
        return 'user_utils';
    }

    protected getFilterableFields(): Array<string> {
        return ['fullname', 'username', 'email'];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {UserUtil}
     */
    protected createNewEntity(data: any): UserUtil {
        return new UserUtil(data);
    }
}
