import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {PaymentCondition} from '../entity/payment-condition';

@Injectable()
export class PaymentConditionRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'payment_conditions';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[]}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {PaymentCondition}
     */
    protected createNewEntity(data: any): PaymentCondition {
        return new PaymentCondition(data);
    }
}
