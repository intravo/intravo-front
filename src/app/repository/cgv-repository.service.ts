import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseRepository} from './base-repository';
import {Cgv} from '../entity/cgv';

@Injectable()
export class CgvRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'c_g_vs';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[]}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {Cgv}
     */
    protected createNewEntity(data: any): Cgv {
        return new Cgv(data);
    }
}

