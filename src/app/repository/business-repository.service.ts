import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseRepository} from './base-repository';
import {Business} from '../entity/business';

@Injectable()
export class BusinessRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'businesses';
    }

    /**
     * removeBusiness()
     *
     * @param id
     * @returns {Observable<Object>}
     */
    public removeBusiness(id: number) {
        return this.remove(id);
    }

    /**
     * getFilterableFields()
     *
     * @returns {[string,string]}
     */
    protected getFilterableFields(): Array<string> {
        return [
            'code',
            'libelle'
        ];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {Business}
     */
    protected createNewEntity(data: any): Business {
        return new Business(data);
    }
}
