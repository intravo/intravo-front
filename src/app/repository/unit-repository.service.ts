import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Unit} from '../entity/unit';
import {BaseRepository} from '../repository/base-repository';

@Injectable()
export class UnitRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'units';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[]}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {Unit}
     */
    protected createNewEntity(data: any): Unit {
        return new Unit(data);
    }
}
