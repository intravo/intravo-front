import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {Role} from '../entity/role';

@Injectable()
export class RolesRepositoryService extends BaseRepository {

    /**
     * @param {HttpClient} http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * @returns {string}
     */
    public getEntityName(): string {
        return 'roles';
    }

    /**
     * @returns {Array<string>}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * @param data
     * @returns {Role}
     */
    protected createNewEntity(data: any): Role {
        return new Role(data);
    }
}
