import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {QuotationLine} from '../entity/quotation-line';
import {BaseRepository} from '../repository/base-repository';

@Injectable()
export class QuotationLineRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'quotation_lines';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[]}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {QuotationLine}
     */
    protected createNewEntity(data: any): QuotationLine {
        return new QuotationLine(data);
    }
}
