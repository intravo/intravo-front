import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {Module} from '../entity/module';

@Injectable()
export class ModuleRepositoryService extends BaseRepository {

    /**
     * Constructor
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * @returns {string}
     */
    public getEntityName(): string {
        return 'modules';
    }

    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {UserUtil}
     */
    protected createNewEntity(data: any): Module {
        return new Module(data);
    }
}
