import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {Etat} from '../entity/etat';

@Injectable()
export class EtatRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'etats';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[]}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {Etat}
     */
    protected createNewEntity(data: any): Etat {
        return new Etat(data);
    }

}
