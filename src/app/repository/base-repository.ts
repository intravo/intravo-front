import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BaseEntity} from '../entity/base-entity';
import {isUndefined} from 'util';

export abstract class BaseRepository {
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/ld+json',
        })
    };
    private _collectionSearch: string;
    private _collectionPage: number;
    private _collectionLastOrderField: string;
    private _collectionOrderField: string;
    private _collectionOrder: string;

    /**
     * Constructor
     *
     * @param http
     */
    constructor(private http: HttpClient) {
        this._collectionSearch = null;
        this._collectionPage = 1;
        this._collectionLastOrderField = null;
        this._collectionOrderField = null;
        this._collectionOrder = null;
    }

    /**
     * @param data
     * @returns {number}
     */
    public static getNbResults(data: any = {}) {
        return typeof data['hydra:totalItems'] !== 'undefined' ? data['hydra:totalItems'] : 0;
    }

    /**
     * @param data
     * @returns {null}
     */
    public static getOne(data: any = {}) {
        return typeof data['hydra:member'][0] !== 'undefined' ? data['hydra:member'][0] : null;
    }

    /**
     * get()
     *
     * @param params
     * @returns {Observable<Object>}
     */
    public get(params: any = {}) {
        return this.http.get(this.buildUrl(params));
    }

    /**
     * @param id
     * @returns {Observable<Object>}
     */
    public findById(id) {
        return this.http.get(environment.apiUrl + this.getEntityName() + '/' + id);
    }

    /**
     * delete()
     *
     * @param params
     * @returns {Observable<Object>}
     */
    public remove(id: number) {
        return this.http.delete(environment.apiUrl + this.getEntityName() + '/' + id);
    }

    /**
     * save()
     *
     * @param data
     * @param entity
     * @returns {any|any[]|Int32Array|Float64Array|Promise<any[]>|wdpromise.Promise<T[]>}
     */

    public save(body, id?: number) {

        if (isUndefined(id) || id == null) {
            return this.http.post(environment.apiUrl + this.getEntityName(), body, this.httpOptions);
        } else {
            return this.http.put(environment.apiUrl + this.getEntityName() + '/' + id, body, this.httpOptions);
        }
    }

    /**
     * mapData()
     *
     * @param data
     * @returns {any|any[]|Int32Array|Float64Array|Promise<any[]>|wdpromise.Promise<T[]>}
     */
    public mapData(data: any = {}) {
        return data['hydra:member'].map(d => this.createNewEntity(d));
    }

    /**
     * getCollection from values of filters, sorting, and page
     *
     * @returns {Observable<Object>}
     */
    public getCollection() {
        const params = {};

        if (this._collectionPage && typeof this._collectionPage === 'number' && this._collectionPage > 0) {
            params['page'] = this._collectionPage;
        }

        if (this._collectionOrder && this._collectionOrderField) {
            params['order[' + this._collectionOrderField + ']'] = this._collectionOrder;
        }

        if (this._collectionSearch && typeof this._collectionSearch === 'string' && this._collectionSearch.trim()) {
            this.getFilterableFields().forEach(fieldName => {
                params[fieldName] = this._collectionSearch;
            });
        }

        this.saveSearch();

        return this.http.get(this.buildUrl(params));
    }

    /**
     * Persist search in session to retrieve later
     */
    saveSearch() {
        const search = {
            collectionPage: this._collectionPage,
            collectionSearch: this._collectionSearch,
            collectionLastOrderField: this._collectionLastOrderField,
            collectionOrderField: this._collectionOrderField,
            collectionOrder: this._collectionOrder,
        };

        localStorage.setItem(this.getEntityName() + 'SavedSearch', JSON.stringify(search));
    }

    /**
     * Retrieve the last saved filters, page and sorting
     */
    retrieveSearch() {
        const search = localStorage.getItem(this.getEntityName() + 'SavedSearch');
        const searchObj = search ? JSON.parse(search) : false;

        if (searchObj) {
            this._collectionPage = searchObj.collectionPage ? searchObj.collectionPage : this._collectionPage;
            this._collectionSearch = searchObj.collectionSearch ? searchObj.collectionSearch : this._collectionSearch;
            this._collectionLastOrderField = searchObj.collectionLastOrderField ? searchObj.collectionLastOrderField
                : this._collectionLastOrderField;
            this._collectionOrderField = searchObj.collectionOrderField ? searchObj.collectionOrderField : this._collectionOrderField;
            this._collectionOrder = searchObj.collectionOrder ? searchObj.collectionOrder : this._collectionOrder;
        }
    }

    /**
     * Set page
     *
     * @param {number} page
     * @returns {this}
     */
    setCollectionPage(page: number) {
        this._collectionPage = page;

        return this;
    }

    /**
     * Set search
     *
     * @param {string} search
     * @returns {this}
     */
    setCollectionSearch(search: string) {
        this._collectionPage = 1;
        this._collectionSearch = search;

        return this;
    }

    /**
     * Change Order by
     *
     * @param field
     */
    changeCollectionOrder(field) {
        this._collectionPage = 1;

        if (this._collectionOrder) {
            if (this._collectionLastOrderField === field) {
                if (this._collectionOrder === 'asc') {
                    this._collectionOrder = 'desc';
                } else if (this._collectionOrder === 'desc') {
                    this._collectionOrder = null;
                    this._collectionOrderField = null;
                    this._collectionLastOrderField = null;
                }
            } else {
                this._collectionOrder = 'asc';
            }
        } else {
            this._collectionOrder = 'asc';
        }

        this._collectionOrderField = field;
        this._collectionLastOrderField = field;
    }

    /**
     * @param data
     * @returns {number[]}
     */
    public getPages(data: any = {}) {
        const numberPages = Math.ceil(data['hydra:totalItems'] / 20);

        return Array(numberPages).fill(0).map((x, i) => i + 1);
    }

    /**
     * @returns {string}
     */
    getCollectionSearch(): string {
        return this._collectionSearch;
    }

    /**
     * @returns {number}
     */
    getCollectionPage(): number {
        return this._collectionPage;
    }

    /**
     * @returns {string}
     */
    getCollectionOrderField(): string {
        return this._collectionOrderField;
    }

    /**
     * @returns {string}
     */
    getCollectionOrder(): string {
        return this._collectionOrder;
    }

    /**
     * createNewEntity()
     */
    protected abstract createNewEntity(data: any): BaseEntity;

    /**
     * getEntityName()
     */
    protected abstract getEntityName(): string;

    /**
     *
     * @returns {Array<string>}
     */
    protected abstract getFilterableFields(): Array<string>;

    /**
     * buildUrl()
     *
     * @param params
     */
    private buildUrl(params: any = {}) {
        let urlParams = '';

        Object.keys(params).forEach((key, index, array) => {

            if (index === 0 && params[key]) {
                urlParams += key + '=' + params[key];
            }

            if (index !== 0 && index <= (array.length - 1) && params[key]) {
                urlParams += '&' + key + '=' + params[key];
            }
        });

        if (urlParams !== '') {
            urlParams = '?' + urlParams;
        }

        return environment.apiUrl + this.getEntityName() + urlParams;
    }
}
