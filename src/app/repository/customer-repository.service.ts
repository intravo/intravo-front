import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {Customer} from '../entity/customer';

@Injectable()
export class CustomerRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'customers';
    }

    /**
     * getFilterableFields()
     *
     * @returns {[string,string,string]}
     */
    protected getFilterableFields(): Array<string> {
        return ['name', 'siret', 'postalCode'];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {Customer}
     */
    protected createNewEntity(data: any): Customer {
        return new Customer(data);
    }
}
