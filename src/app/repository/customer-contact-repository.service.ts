import {Injectable} from '@angular/core';
import {BaseRepository} from './base-repository';
import {HttpClient} from '@angular/common/http';
import {CustomerContact} from '../entity/customer-contact';

@Injectable()
export class CustomerContactRepositoryService extends BaseRepository {

    /**
     * constructor()
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     * getEntityName()
     *
     * @returns {string}
     */
    public getEntityName(): string {
        return 'contacts';
    }

    /**
     * getCustomerContact()
     *
     * @param params
     * @returns {Observable<Object>}
     */
    public getCustomerContact(params: any = {}) {
        return this.get(params);
    }

    /**
     *
     * @param entity
     * @returns {Observable<Object>|Observable<Object>}
     */
    saveCustomerContact(entity: CustomerContact) {
        return this.save(entity, entity.id);
    }

    /**
     * getFilterableFields()
     *
     * @returns {Array}
     */
    protected getFilterableFields(): Array<string> {
        return [];
    }

    /**
     * createNewEntity()
     *
     * @param data
     * @returns {CustomerContact}
     */
    protected createNewEntity(data: any): CustomerContact {
        return new CustomerContact(data);
    }
}
