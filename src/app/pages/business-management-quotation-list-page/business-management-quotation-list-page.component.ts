import {Component, OnInit} from '@angular/core';
import {QuotationRepositoryService} from '../../repository/quotation-repository.service';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';
import {GridConfig} from '../../entity/grid/grid-config';

@Component({
    selector: 'app-business-management-quotation-list-page',
    templateUrl: './business-management-quotation-list-page.component.html',
    styleUrls: ['./business-management-quotation-list-page.component.scss']
})
export class BusinessManagementQuotationListPageComponent implements OnInit {

    gridQuotation: GridConfig;

    /**
     * @param {QuotationRepositoryService} quotationRepository
     */
    constructor(private quotationRepository: QuotationRepositoryService) {

    }

    /**
     * ngOnInit()
     */
    ngOnInit() {
        this.gridQuotation = new GridConfig([
            new FieldGridConfig('Code Devis', 'code', true),
            new FieldGridConfig('Intitulé devis', 'title', true),
            new FieldGridConfig('Client', 'customer', false, (customer) => customer.name),
            new FieldGridConfig('Affaire', 'business', false, (business) => business.code),
            new FieldGridConfig('Type', 'benefit', false, (benefit) => benefit.title),
            new FieldGridConfig('Prix HT', 'totalPriceHt', true, price => price + ' €'),
            new FieldGridConfig('Etat', 'etat', false, (etat) => etat.title),
        ]);
        this.gridQuotation.detailsRoute = '/business/quotation/details';
    }
}
