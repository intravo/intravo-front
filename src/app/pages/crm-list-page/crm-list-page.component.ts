import {Component, OnInit} from '@angular/core';
import {CustomerRepositoryService} from '../../repository/customer-repository.service';
import {GridConfig} from '../../entity/grid/grid-config';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';

@Component({
    selector: 'app-crm-list-page',
    templateUrl: './crm-list-page.component.html',
    styleUrls: ['./crm-list-page.component.scss']
})
export class CrmListPageComponent implements OnInit {

    gridConfig: GridConfig;

    constructor(public customerRepository: CustomerRepositoryService) {
    }

    ngOnInit() {
        this.gridConfig = new GridConfig([
            new FieldGridConfig('Code client', 'code', false),
            new FieldGridConfig('Nom', 'name', true),
            new FieldGridConfig('Code postal', 'postalCode', true),
            new FieldGridConfig('Ville', 'city', true)
        ]);
        this.gridConfig.detailsRoute = '/crm/details';
    }
}
