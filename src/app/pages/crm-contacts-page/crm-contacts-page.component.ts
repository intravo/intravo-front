import {Component, OnInit} from '@angular/core';
import {CustomerContactRepositoryService} from '../../repository/customer-contact-repository.service';
import {CustomerContact} from '../../entity/customer-contact';
import {isUndefined} from 'util';
import {DialogAddContactComponent} from '../customer-contact/dialog-add-contact/dialog-add-contact.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';
import {GridConfig} from '../../entity/grid/grid-config';

@Component({
    selector: 'app-crm-contacts-page',
    templateUrl: './crm-contacts-page.component.html',
    styleUrls: ['./crm-contacts-page.component.scss']
})
export class CrmContactsPageComponent implements OnInit {

    addDialog: MatDialogRef<DialogAddContactComponent>;
    editContact: CustomerContact;
    gridConfig: GridConfig;

    /**
     * @param {MatDialog} dialog
     * @param {CustomerContactRepositoryService} contactRepository
     */
    constructor(public dialog: MatDialog, public contactRepository: CustomerContactRepositoryService) {
    }

    ngOnInit() {
        this.gridConfig = new GridConfig([
            new FieldGridConfig('Prénom', 'firstName', false),
            new FieldGridConfig('Nom', 'lastName', true),
            new FieldGridConfig('Fonction', 'fonction', false, fonction => fonction.title),
            new FieldGridConfig('Téléphone', 'phone', false),
            new FieldGridConfig('Téléphone portable', 'mobilePhone', true),
            new FieldGridConfig('Email', 'email', true),
        ]);
        this.gridConfig.detailsAction = false;
        this.gridConfig.addCustomAction('pencil-alt', (contact) => {
            this.openDialog(contact);
        });
    }

    /**
     * @param {CustomerContact} contact
     */
    openDialog(contact?: CustomerContact): void {

        if (!isUndefined(contact)) {
            this.editContact = contact;
        } else {
            this.editContact = new CustomerContact({});
        }

        this.addDialog = this.dialog.open(DialogAddContactComponent, {
            height: '450px',
            width: '600px',
            data: this.editContact
        });
    }
}
