import {Component, OnInit} from '@angular/core';
import {BusinessRepositoryService} from '../../repository/business-repository.service';
import {Router} from '@angular/router';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';
import {GridConfig} from '../../entity/grid/grid-config';

@Component({
    selector: 'app-business-management-business-list-page',
    templateUrl: './business-management-business-list-page.component.html',
    styleUrls: ['./business-management-business-list-page.component.scss']
})
export class BusinessManagementBusinessListPageComponent implements OnInit {

    private gridConfig: GridConfig;

    /**
     * @param {Router} router
     * @param {BusinessRepositoryService} businessRepository
     */
    constructor(private router: Router,
                private businessRepository: BusinessRepositoryService) {
    }

    ngOnInit() {
        this.gridConfig = new GridConfig([
            new FieldGridConfig('Code affaire', 'code', true),
            new FieldGridConfig('Nom de l\'affaire', 'libelle', true),
            new FieldGridConfig('Client', 'customer', false, customer => customer.name),
        ]);
        this.gridConfig.detailsRoute = '/business/details';
    }
}
