import {Component, Input, OnInit} from '@angular/core';
import {BaseRepository} from '../../../repository/base-repository';
import {Referential} from '../../../entity/Referential';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-referential',
    templateUrl: './referential.component.html',
    styleUrls: ['./referential.component.scss']
})
export class ReferentialComponent implements OnInit {
    @Input() nameList: string;
    @Input() service: BaseRepository;

    loader: boolean;
    entities: Referential[] = [];
    formGroups: FormGroup[] = [];
    entity: Referential = new Referential();

    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
        this.loader = true;
        this.load();
    }

    load() {
        this.service.get().subscribe(item => {
            this.entities = this.service.mapData(item);
            this.entities.push(new Referential());
            this.buildForm();
            this.loader = false;
        });
    }

    /**
     * @param {number} id
     */
    remove(id: number) {
        this.service.remove(id).subscribe();
    }

    /**
     * @param id
     */
    save(id?) {
        this.service.save(this.formGroups[id].value, (id !== 'new' ? id : undefined)).subscribe(() => {
            this.load();
        });
    }

    buildForm() {
        this.entities.map(entity => {
            this.formGroups[(entity.id ? entity.id.toString() : 'new')] = entity.buildForm(this.fb);
        });
    }
}
