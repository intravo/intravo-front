import {Component, OnInit} from '@angular/core';
import {TypeBenefitRepositoryService} from '../../repository/type-benefit-repository.service';
import {CgvRepositoryService} from '../../repository/cgv-repository.service';
import {EtatRepositoryService} from '../../repository/etat-repository.service';
import {FunctionContactService} from '../../repository/function-contact.service';
import {UnitRepositoryService} from '../../repository/unit-repository.service';

@Component({
    selector: 'app-settings-page',
    templateUrl: './settings-page.component.html',
    styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent implements OnInit {


    constructor(public benefitServ: TypeBenefitRepositoryService,
                public cgvServ: CgvRepositoryService,
                public etatServ: EtatRepositoryService,
                public functionServ: FunctionContactService,
                public  unitServ: UnitRepositoryService) {
    }

    ngOnInit() {
    }
}
