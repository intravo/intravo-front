export abstract class AccessControl {

    /**
     * checkPermission()
     *
     * @param access
     */
    public abstract checkPermission(access: boolean): boolean;
}