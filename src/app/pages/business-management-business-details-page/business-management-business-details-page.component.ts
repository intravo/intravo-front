import {Component, OnInit} from '@angular/core';
import {BusinessRepositoryService} from '../../repository/business-repository.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Business} from '../../entity/business';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isUndefined} from 'util';
import {CustomerRepositoryService} from '../../repository/customer-repository.service';
import {Customer} from '../../entity/customer';
import {CustomerContactRepositoryService} from '../../repository/customer-contact-repository.service';
import {CustomerContact} from '../../entity/customer-contact';
import {GridConfig} from '../../entity/grid/grid-config';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';

@Component({
    selector: 'app-business-management-business-details-page',
    templateUrl: './business-management-business-details-page.component.html',
    styleUrls: ['./business-management-business-details-page.component.scss']
})
export class BusinessManagementBusinessDetailsPageComponent implements OnInit {

    businessForm: FormGroup;
    business: Business;
    customerList: Customer[];
    showDetail: boolean;
    businessId: number;
    selectedCustomer: Customer;
    contactList: CustomerContact[];
    gridQuotation: GridConfig;

    constructor(private service: BusinessRepositoryService,
                private customerRepositoryService: CustomerRepositoryService,
                private contactRepositoryService: CustomerContactRepositoryService,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private router: Router) {
        this.customerList = [];
        this.contactList = [];
        this.selectedCustomer = null;
    }

    onSubmit() {
        this.business = this.businessForm.value as Business;

        this.service.save(this.business, this.businessId).subscribe(data => {
            if (data['id']) {
                this.service.save(this.business, data['id']).subscribe(resp => {
                    this.router.navigate(['details', resp['id']]);
                });
            }
        });
    }

    ngOnInit() {
        this.customerRepositoryService.get().subscribe((data) => {
            this.customerList = this.customerRepositoryService.mapData(data);
        });

        this.route.params.subscribe(params => {
            this.businessId = params['id'];
        });

        if (!isUndefined(this.businessId)) {
            this.service.findById(this.businessId).subscribe(data => {
                this.business = new Business(data);
                this.selectedCustomer = this.business.customer;
                this.buildParam();
                this.showDetail = true;

                this.gridQuotation = new GridConfig([
                    new FieldGridConfig('Code Devis', 'code'),
                    new FieldGridConfig('Intitulé devis', 'title'),
                    new FieldGridConfig('Affaires', 'business', false, (business) => business.code),
                    new FieldGridConfig('Type', 'benefit', false, (benefit) => benefit.title),
                    new FieldGridConfig('Prix HT', 'totalPriceHt'),
                    new FieldGridConfig('Etat', 'etat', false, (etat) => etat.title),
                ]);
                this.gridQuotation.detailsRoute = '/business/quotation/details';
                this.gridQuotation.removeAction = false;
            });
        } else {
            this.business = new Business({});
            this.showDetail = true;
            this.buildParam();
        }
    }

    buildParam() {
        this.selectedCustomer = this.business.customer;
        this.businessForm = this.formBuilder.group({
            'code': [this.business.code, Validators.required],
            'libelle': [this.business.libelle, Validators.required],
            'note': [this.business.note, Validators.required],
            'customer': [this.business.customer, Validators.required],
            'contact_com': [this.business.contact_com, Validators.required],
            'contact_tec': [this.business.contact_tec, Validators.required],
            'file': [this.business.file, Validators.required],
        });
    }
}
