import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerRepositoryService} from '../../repository/customer-repository.service';
import {isNullOrUndefined} from 'util';
import {Customer} from '../../entity/customer';
import {NotificationsService} from '../../services/notifications.service';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';
import {GridConfig} from '../../entity/grid/grid-config';

@Component({
    selector: 'app-crm-details-page',
    templateUrl: './crm-details-page.component.html',
    styleUrls: ['./crm-details-page.component.scss']
})
export class CrmDetailsPageComponent implements OnInit {

    addUserForm: FormGroup;
    customers: Customer[] = [];
    customer: Customer;
    showDetail: boolean;
    customerId: number;
    loader: boolean;
    isNewCustomer: boolean;
    gridContacts: GridConfig;
    gridBusiness: GridConfig;
    gridQuotation: GridConfig;

    constructor(private formBuilder: FormBuilder,
                private customerService: CustomerRepositoryService,
                private route: ActivatedRoute,
                private router: Router,
                private notificationService: NotificationsService) {
        this.loader = true;
    }

    onSubmit() {
        this.loader = true;
        this.customer = this.addUserForm.value as Customer;
        this.customerService.save(this.customer, this.customerId).subscribe(data => {
            if (data['id']) {
                if (!data['code']) {
                    const tmp: string = '0000' + data['id'];
                    this.customer.code = 'CLI' + tmp.substring(tmp.length - 4, tmp.length);
                }

                this.customerService.save(this.customer, data['id']).subscribe(resp => {
                    if (this.isNewCustomer) {
                        this.notificationService.success('Client créée avec succès !');
                        this.router.navigate(['crm/details', resp['id']]);
                    } else {
                        this.notificationService.success('Client mis à jour avec succès !');
                        this.loader = false;
                    }
                });
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.customerId = params.id;

            if (!isNullOrUndefined(this.customerId)) {
                this.customerService.findById(this.customerId).subscribe(data => {
                    this.customer = new Customer(data);
                    this.buildForm();
                    this.showDetail = true;
                    this.isNewCustomer = false;

                    this.buildGrids();
                });
            } else {
                this.isNewCustomer = true;
                this.customer = new Customer();
                this.showDetail = true;
                this.buildForm();
            }
        });
    }

    buildForm() {
        this.addUserForm = this.formBuilder.group({
            'code': [this.customer.code, Validators.required],
            'name': [this.customer.name, Validators.required],
            'socialreason': [this.customer.socialreason, Validators.required],
            'siret': [this.customer.siret, Validators.required],
            'address': [this.customer.address, Validators.required],
            'addressComplement': [this.customer.addressComplement, Validators.required],
            'postalCode': [this.customer.postalCode, Validators.required],
            'city': [this.customer.city, Validators.required],
            'phone': [this.customer.phone, Validators.required],
            'mobile': [this.customer.mobile, Validators.required],
            'memo': [this.customer.memo, Validators.required],
        });
        this.loader = false;
    }

    buildGrids() {
        // Contacts
        this.gridContacts = new GridConfig([
            new FieldGridConfig('Prénom', 'firstName'),
            new FieldGridConfig('Nom', 'lastName', true),
            new FieldGridConfig('Fonction', 'fonction', false, fonction => fonction.title),
            new FieldGridConfig('Téléphone', 'phone'),
            new FieldGridConfig('Téléphone portable', 'mobilePhone', true),
            new FieldGridConfig('Email', 'email', true),
        ]);
        this.gridContacts.detailsAction = false;
        this.gridContacts.removeAction = false;

        // Business
        this.gridBusiness = new GridConfig([
            new FieldGridConfig('Code affaire', 'code'),
            new FieldGridConfig('Nom de l\'affaire', 'libelle'),
        ]);
        this.gridBusiness.detailsRoute = '/business/details';
        this.gridBusiness.removeAction = false;

        // Quotation
        this.gridQuotation = new GridConfig([
            new FieldGridConfig('Code Devis', 'code'),
            new FieldGridConfig('Intitulé devis', 'title'),
            new FieldGridConfig('Affaires', 'business', false, (business) => business.code),
            new FieldGridConfig('Type', 'benefit', false, (benefit) => benefit.title),
            new FieldGridConfig('Prix HT', 'totalPriceHt'),
            new FieldGridConfig('Etat', 'etat', false, (etat) => etat.title),
        ]);
        this.gridQuotation.detailsRoute = '/business/quotation/details';
        this.gridQuotation.removeAction = false;
    }
}
