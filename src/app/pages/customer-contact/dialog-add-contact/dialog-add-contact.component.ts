import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CustomerContact} from '../../../entity/customer-contact';
import {CustomerContactRepositoryService} from '../../../repository/customer-contact-repository.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FunctionContact} from '../../../entity/function-contact';
import {FunctionContactService} from '../../../repository/function-contact.service';

@Component({
    selector: 'app-dialog-add-contact',
    templateUrl: './dialog-add-contact.component.html',
    styleUrls: ['./dialog-add-contact.component.scss']
})
export class DialogAddContactComponent implements OnInit {
    contactForm: FormGroup;

    fonctions: FunctionContact[] = [];
    addContact: CustomerContact = new CustomerContact({});

    constructor(public dialogRef: MatDialogRef<DialogAddContactComponent>,
                @Inject(MAT_DIALOG_DATA) public data: CustomerContact,
                private contactServ: CustomerContactRepositoryService,
                private fb: FormBuilder,
                private foncServ: FunctionContactService) {
    }

    ngOnInit() {
        this.foncServ.getFoncContact().subscribe(data => {
            data['hydra:member'].map(x => {
                this.fonctions.push(new FunctionContact(x));
            });
        });

        // Customer est undefined
        this.contactForm = this.fb.group({
            id: [this.data.id],
            firstName: [this.data.firstName],
            fonction: [this.data.fonction],
            lastName: [this.data.lastName],
            phone: [this.data.phone],
            mobilePhone: [this.data.mobilePhone],
            email: [this.data.email],
            customer: [this.data.customer]
        });
    }

    onSubmit() {
        this.addContact.setData(this.contactForm.value);
        this.addContact.customer = this.data.customer;

        this.contactServ.saveCustomerContact(this.addContact).subscribe(data => {
        });
    }
}
