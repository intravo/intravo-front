import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserUtil} from '../../entity/user-util';
import {NotificationsService} from '../../services/notifications.service';
import {UserUtilRepositoryService} from '../../repository/user-util-repository.service';
import {ActivatedRoute, Router} from '@angular/router';
import {isUndefined} from 'util';
import {RolesRepositoryService} from '../../repository/roles-repository.service';
import {Role} from '../../entity/role';

@Component({
    selector: 'app-user-details-page',
    templateUrl: './user-details-page.component.html',
    styleUrls: ['./user-details-page.component.scss']
})
export class UserDetailsPageComponent implements OnInit {

    userUtil: UserUtil;
    roles: Role[];
    addUserUtilForm: FormGroup;
    loader: boolean;

    constructor(private formBuilder: FormBuilder,
                private userUtilRepository: UserUtilRepositoryService,
                private roleRepository: RolesRepositoryService,
                private route: ActivatedRoute,
                private router: Router,
                private notificationService: NotificationsService) {
    }

    ngOnInit() {
        this.loader = true;
        this.userUtil = new UserUtil();

        this.route.params.subscribe(params => {
            this.roleRepository.get().subscribe(roleData => {
                this.roles = this.roleRepository.mapData(roleData);

                if (!isUndefined(params.id)) {
                    this.userUtilRepository.findById(params.id).subscribe(data => {
                        this.userUtil = new UserUtil(data);
                        this.buildForm();
                        this.loader = false;
                    });
                } else {
                    this.userUtil = new UserUtil();
                    this.buildForm();
                    this.loader = false;
                }
            });
        });
    }

    onSubmit() {
        this.loader = true;

        if (this.userUtil.id) {
            this.userUtilRepository.save(this.userUtil, this.userUtil.id).subscribe(data => {
                this.userUtil = new UserUtil(data);
                this.notificationService.success('Utilisateur mis à jour avec succès !');
                this.buildForm();
                this.loader = false;
            });
        } else {
            this.userUtil = this.addUserUtilForm.value as UserUtil;
            this.userUtilRepository.save(this.userUtil).subscribe(data => {
                this.userUtil = new UserUtil(data);
                this.notificationService.success('Utilisateur créée avec succès !');
                this.router.navigate(['administration/user/details', this.userUtil.id]);
            });
        }
    }

    buildForm() {
        this.userUtil.role = this.roles.filter(r => r.id === this.userUtil.role.id)[0];
        this.addUserUtilForm = this.formBuilder.group({
            'email': [this.userUtil.email, Validators.required],
            'username': [this.userUtil.username, Validators.required],
            'role': [this.userUtil.role, Validators.required],
            'fullname': [this.userUtil.fullname, Validators.required],
            'plainPassword': ['', Validators.required],
        });
    }
}
