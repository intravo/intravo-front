import {Component, OnInit} from '@angular/core';
import {UserUtilRepositoryService} from '../../repository/user-util-repository.service';
import {FieldGridConfig} from '../../entity/grid/field-grid-config';
import {GridConfig} from '../../entity/grid/grid-config';

@Component({
    selector: 'app-user-list-page',
    templateUrl: './user-list-page.component.html',
    styleUrls: ['./user-list-page.component.scss']
})
export class UserListPageComponent implements OnInit {

    gridConfig: GridConfig;

    constructor(public userRepository: UserUtilRepositoryService) {
    }

    ngOnInit() {
        this.gridConfig = new GridConfig([
            new FieldGridConfig('Id', 'id'),
            new FieldGridConfig('Nom complet', 'fullname', true),
            new FieldGridConfig('Nom d\'utilisateur', 'username', true),
            new FieldGridConfig('Email', 'email', true),
            new FieldGridConfig('Role', 'role', false, role => role.title),
        ]);
        this.gridConfig.detailsRoute = '/administration/user/details';
    }
}
