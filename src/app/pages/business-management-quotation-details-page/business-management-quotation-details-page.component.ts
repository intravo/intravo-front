import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Quotation} from '../../entity/quotation';
import {QuotationRepositoryService} from '../../repository/quotation-repository.service';
import {NotificationsService} from '../../services/notifications.service';
import {isNullOrUndefined} from 'util';
import {QuotationLineRepositoryService} from '../../repository/quotation-line-repository.service';
import {Unit} from '../../entity/unit';

@Component({
    selector: 'app-business-management-quotation-details-page',
    templateUrl: './business-management-quotation-details-page.component.html',
    styleUrls: ['./business-management-quotation-details-page.component.scss']
})
export class BusinessManagementQuotationDetailsPageComponent implements OnInit {

    public isSaving = false;
    public quotation: Quotation;

    /**
     * @param {ActivatedRoute} activatedRoute
     * @param {QuotationRepositoryService} quotationRepository
     */
    constructor(private activatedRoute: ActivatedRoute,
                private quotationRepository: QuotationRepositoryService,
                private quotationLineRepository: QuotationLineRepositoryService,
                private router: Router,
                private notificationService: NotificationsService) {
        this.quotation = new Quotation();
    }

    /**
     * ngOnInit()
     */
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            if (params['id']) {
                this.quotationRepository.findById(params['id']).subscribe((data) => {
                    this.quotation = (data) ? new Quotation(data) : new Quotation();
                });
            }
        });
    }

    save() {
        this.isSaving = true;

        if (!this.isFormValid()) {
            this.notificationService.error('Une ou plusieurs lignes de devis ne sont pas totalement renseignées');
            this.isSaving = false;
            return;
        }

        if (this.quotation.id) {
            this.quotationRepository.save(this.quotation, this.quotation.id).subscribe(data => {
                this.quotation = new Quotation(data);
                this.notificationService.success('Devis mis à jour avec succès !');
                this.isSaving = false;
            });
        } else {
            this.quotationRepository.save(this.quotation).subscribe(data => {
                this.quotation = new Quotation(data);
                this.notificationService.success('Devis créé avec succès !');
                this.isSaving = false;
                this.router.navigate(['business/quotation/details', this.quotation.id]);
            });
        }
    }

    isFormValid() {
        let formIsValid = true;

        this.quotation.quotationLines.forEach(line => {
            if (line.designation.trim() === '' || isNullOrUndefined(line.priceHT) || isNullOrUndefined(line.quantity)) {
                formIsValid = false;
            }

            if (!line.isTitle) {
                if (!(line.unit instanceof Unit)) {
                    formIsValid = false;
                }

                if (isNullOrUndefined(line.priceHT)) {
                    formIsValid = false;
                }

                if (isNullOrUndefined(line.quantity)) {
                    formIsValid = false;
                }
            }
        });

        return formIsValid;
    }
}
