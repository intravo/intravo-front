import {Component, Input, OnInit} from '@angular/core';
import {QuotationLine} from '../../../../../entity/quotation-line';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Unit} from '../../../../../entity/unit';
import {Quotation} from '../../../../../entity/quotation';
import {NotificationsService} from '../../../../../services/notifications.service';

@Component({
    selector: 'tr[app-line]',
    templateUrl: './line.component.html',
    styleUrls: ['./line.component.scss']
})
export class LineComponent implements OnInit {

    @Input() unitList: Unit[];
    @Input() quotationLine: QuotationLine;
    @Input() quotation: Quotation;
    private quotationLineForm: FormGroup;
    private selectedUnit: Unit;

    /**
     * constructor()
     *
     * @param formBuilder
     * @param notification
     */
    constructor(private formBuilder: FormBuilder,
                private notification: NotificationsService) {
    }

    /**
     * ngOnInit()
     */
    ngOnInit() {
        this.quotationLine = (this.quotationLine) ? this.quotationLine : new QuotationLine();
        this.buildForm();
    }

    /**
     * Offer the line
     */
    gift() {
        this.quotationLine.isGift = !this.quotationLine.isGift;
        this.quotationLine.calculatePrices();
        this.quotation.calculatePrices();
    }

    /**
     * Set the line as a title
     */
    title() {
        this.quotationLine.isTitle = !this.quotationLine.isTitle;
        this.quotationLine.calculatePrices();
        this.quotation.calculatePrices();
    }

    /**
     * Remove the line
     */
    remove() {
        this.quotation.removeLine(this.quotationLine);
    }

    /**
     * Up the line in the list
     */
    up() {
        this.quotation.upLine(this.quotationLine);
    }

    /**
     * Down the line in the list
     */
    down() {
        this.quotation.downLine(this.quotationLine);
    }

    /**
     * On line changes, update prices
     */
    onChanges(): void {
        this.quotationLineForm.valueChanges.subscribe(val => {
            this.quotationLine.designation = val.designation;
            this.quotationLine.quantity = parseInt(val.quantity, 10);
            this.quotationLine.priceHT = parseFloat(val.priceHT);
            this.quotationLine.position = parseInt(val.position, 10);
            this.quotationLine.unit = val.unit;
            this.quotationLine.calculatePrices();
            this.quotation.calculatePrices();
        });
    }

    /**
     * buildForm
     */
    private buildForm() {
        this.selectedUnit = this.unitList.filter(elem => this.quotationLine.unit.id === elem.id)[0];
        this.quotationLineForm = this.formBuilder.group({
            id: [this.quotationLine.id],
            designation: [this.quotationLine.designation, Validators.required],
            quantity: [this.quotationLine.quantity, Validators.required],
            priceHT: [this.quotationLine.priceHT, Validators.required],
            position: [this.quotationLine.position, Validators.required],
            isOptional: [this.quotationLine.isOptional, Validators.required],
            unit: [this.selectedUnit, Validators.required],
        });
        this.onChanges();
    }
}
