import {Component, Input, OnInit} from '@angular/core';
import {Quotation} from '../../../../entity/quotation';
import {QuotationRepositoryService} from '../../../../repository/quotation-repository.service';
import {QuotationLine} from '../../../../entity/quotation-line';
import {Unit} from '../../../../entity/unit';
import {UnitRepositoryService} from '../../../../repository/unit-repository.service';

@Component({
    selector: 'app-quotation-principal',
    templateUrl: './quotation-principal.component.html',
    styleUrls: ['./quotation-principal.component.scss']
})
export class QuotationPrincipalComponent implements OnInit {

    public unitList: Unit[];
    @Input() quotation: Quotation;
    @Input() isSaving: boolean;
    private loader: boolean;

    /**
     * constructor()
     *
     * @param quotationLineRepository
     * @param unitRepositoryService
     */
    constructor(public quotationLineRepository: QuotationRepositoryService,
                private unitRepositoryService: UnitRepositoryService) {
        this.loader = true;
    }

    ngOnInit() {
        this.unitRepositoryService.get().subscribe((data) => {
            this.unitList = this.unitRepositoryService.mapData(data);
            this.loader = false;
        });
    }

    addLine() {
        this.quotation.addLine(new QuotationLine());
    }
}
