import {Component, Input, OnInit} from '@angular/core';
import {Quotation} from '../../../../entity/quotation';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CgvRepositoryService} from '../../../../repository/cgv-repository.service';
import {PaymentConditionRepositoryService} from '../../../../repository/payment-condition-repository.service';
import {TypeBenefitRepositoryService} from '../../../../repository/type-benefit-repository.service';
import {EtatRepositoryService} from '../../../../repository/etat-repository.service';
import {CustomerRepositoryService} from '../../../../repository/customer-repository.service';
import {Customer} from '../../../../entity/customer';
import {TypeBenefit} from '../../../../entity/type-benefit';
import {PaymentCondition} from '../../../../entity/payment-condition';
import {Cgv} from '../../../../entity/cgv';
import {Etat} from '../../../../entity/etat';
import {Business} from '../../../../entity/business';
import {Contact} from '../../../../entity/contact';
import {Router} from '@angular/router';
import {NotificationsService} from '../../../../services/notifications.service';
import {QuotationRepositoryService} from '../../../../repository/quotation-repository.service';
import {BusinessRepositoryService} from '../../../../repository/business-repository.service';

@Component({
    selector: 'app-general-information-widget',
    templateUrl: './general-information-widget.component.html',
    styleUrls: ['./general-information-widget.component.scss']
})
export class GeneralInformationWidgetComponent implements OnInit {

    public form: FormGroup;
    public customerList: Customer[];
    public etatList: Etat[];
    public typeBenefitList: TypeBenefit[];
    public paymentConditionList: PaymentCondition[];
    public businessList: Business[];
    public cgvList: Cgv[];
    public randCode: any;
    public cgvInputVisibility: boolean;
    public selectedBusiness: Business[];
    public comContact: Contact;
    public techContact: Contact;
    public loader: boolean;

    @Input() quotation: Quotation;
    @Input() isSaving: boolean;

    /**
     * constructor()
     *
     * @param formBuilder
     * @param customerRepositoryService
     * @param quotationRepository
     * @param etatRepository
     * @param typeBenefitRepository
     * @param paymentConditionRepository
     * @param cgvRepositoryService
     * @param businessRepository
     * @param route
     * @param router
     * @param notificationService
     */
    constructor(private formBuilder: FormBuilder,
                private customerRepositoryService: CustomerRepositoryService,
                private quotationRepository: QuotationRepositoryService,
                private etatRepository: EtatRepositoryService,
                private typeBenefitRepository: TypeBenefitRepositoryService,
                private paymentConditionRepository: PaymentConditionRepositoryService,
                private cgvRepositoryService: CgvRepositoryService,
                private businessRepository: BusinessRepositoryService,
                private router: Router,
                private notificationService: NotificationsService) {
        this.randCode = Math.floor(Math.random() * 1000);
        this.cgvInputVisibility = true;
        this.comContact = new Contact({});
        this.techContact = new Contact({});
        this.loader = true;
    }

    /**
     * Generate random quotation code
     *
     * @return {string}
     */
    static quotationCode() {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    /**
     * ngOnInit()
     */
    ngOnInit() {
        // On veut être certain que tout soit fini et correctement chargé, d'où cette imbrication de subscribe
        this.etatRepository.get().subscribe((el) => {
            this.cgvRepositoryService.get().subscribe((cgvl) => {
                this.businessRepository.get().subscribe((bl) => {
                    this.paymentConditionRepository.get().subscribe((pcl) => {
                        this.customerRepositoryService.get().subscribe((c) => {
                            this.typeBenefitRepository.get().subscribe((tbl) => {
                                this.customerList = this.customerRepositoryService.mapData(c);
                                this.etatList = this.etatRepository.mapData(el);
                                this.paymentConditionList = this.paymentConditionRepository.mapData(pcl);
                                this.businessList = this.businessRepository.mapData(bl);
                                this.cgvList = this.cgvRepositoryService.mapData(cgvl);
                                this.typeBenefitList = this.typeBenefitRepository.mapData(tbl);

                                if (this.quotation.id) {
                                    this.quotationRepository.findById(this.quotation.id).subscribe((data) => {
                                        this.quotation.setData(data);
                                        this.createForm();
                                        this.loader = false;
                                    });
                                } else {
                                    this.createForm();
                                    this.loader = false;
                                }
                            });
                        });
                    });
                });
            });
        });
    }

    /**
     * onSubmit()
     *
     * Called if the form is submited
     *
     * @return void
     */
    public onSubmit(): void {

        this.loader = true;

        if (this.quotation.id) {
            this.quotationRepository.save(this.form.value, this.quotation.id).subscribe((data) => {
                this.quotation.setData(data);
                this.notificationService.success('Devis modifié avec succès !');
                this.router.navigate(['business/quotation/list']);
            });
        } else {
            this.quotationRepository.save(this.quotation).subscribe((data) => {
                this.quotation.setData(data);
                this.notificationService.success('Devis créée avec succès !');
                this.router.navigate(['business/quotation/list']);
            });
        }
    }

    /**
     * showCgvInput()
     *
     * Show cgv input
     */
    public showCgvInput() {
        this.cgvInputVisibility = true;
    }

    /**
     * hiddenCgvInput()
     *
     * Hidden cgv input
     */
    public hiddenCgvInput() {
        this.cgvInputVisibility = false;
    }

    /**
     * getSelectedBusiness()
     *
     * Get business object selected in form
     *
     * @param buisness
     * @return void
     */
    public getSelectedBusiness(buisness: Business): void {
        if (typeof buisness !== 'undefined') {
            this.comContact = buisness.contact_com;
        }

        if (typeof buisness !== 'undefined') {
            this.techContact = buisness.contact_tec;
        }
    }

    /**
     * createForm()
     *
     * Build the quotation form
     *
     * @return void
     */
    private createForm(): void {

        this.form = this.formBuilder.group({
            'code': [GeneralInformationWidgetComponent.quotationCode()],
            'business': [this.quotation.business],
            'title': [this.quotation.title, Validators.required],
            'creation': [this.quotation.creation, Validators.required],
            'customer': [this.quotation.customer],
            'validity': [this.quotation.validity, Validators.required],
            'customerNote': [this.quotation.customerNote, Validators.required],
            'internalNote': [this.quotation.internalNote, Validators.required],
            'etat': [this.quotation.etat, Validators.required],
            'paymentCondition': [this.quotation.paymentCondition, Validators.required],
            'cgv': [this.quotation.cgv],
            'benefit': [this.quotation.benefit, Validators.required],
        });

        this.loader = false;
    }
}
