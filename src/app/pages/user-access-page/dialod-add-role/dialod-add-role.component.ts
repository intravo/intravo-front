import {Component, OnInit} from '@angular/core';
import {RolesRepositoryService} from '../../../repository/roles-repository.service';
import {MatDialogRef} from '@angular/material';
import {NotificationsService} from '../../../services/notifications.service';
import {UserAccessPageComponent} from '../user-access-page.component';
import {Role} from '../../../entity/role';

@Component({
    selector: 'app-dialod-add-role',
    templateUrl: './dialod-add-role.component.html',
    styleUrls: ['./dialod-add-role.component.scss']
})
export class DialodAddRoleComponent implements OnInit {

    roleName: string;
    userAccessComponent: UserAccessPageComponent;

    constructor(public dialogRef: MatDialogRef<DialodAddRoleComponent>,
                private roleRepository: RolesRepositoryService,
                private notificationService: NotificationsService) {
    }

    ngOnInit() {
        this.roleName = '';
        this.userAccessComponent = this.dialogRef.componentInstance['userAccessComponent'];
    }

    save() {
        if (this.roleName.trim() === '') {
            this.notificationService.error('Veuillez saisir un nom de role valide.');
            return;
        }

        const role = new Role({
            title: this.roleName
        });

        this.roleRepository.save(role).subscribe(roleData => {
            this.dialogRef.close();
            this.userAccessComponent.addRole(new Role(roleData));
            this.notificationService.success('Role créée avec succès');
        });
    }
}
