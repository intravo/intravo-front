import {Component, OnInit} from '@angular/core';
import {RolesRepositoryService} from '../../repository/roles-repository.service';
import {Role} from '../../entity/role';
import {Module} from '../../entity/module';
import {ModuleRepositoryService} from '../../repository/module-repository.service';
import {NotificationsService} from '../../services/notifications.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DialodAddRoleComponent} from './dialod-add-role/dialod-add-role.component';

@Component({
    selector: 'app-user-access-page',
    templateUrl: './user-access-page.component.html',
    styleUrls: ['./user-access-page.component.scss']
})
export class UserAccessPageComponent implements OnInit {

    roles: Role[];
    loader: boolean;
    modules: Module[];
    dialogRef: MatDialogRef<DialodAddRoleComponent>;

    constructor(private roleRepository: RolesRepositoryService,
                private moduleRepository: ModuleRepositoryService,
                private notificationService: NotificationsService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.loader = true;
        this.roleRepository.get().subscribe(roleData => {
            this.moduleRepository.get().subscribe(moduleData => {
                this.roles = this.roleRepository.mapData(roleData);
                this.modules = this.moduleRepository.mapData(moduleData);
                this.loader = false;
            });
        });
    }

    /**
     * @param {Role} role
     * @param {Module} module
     */
    changeStateModuleAccess(role: Role, module: Module) {

        if (role.findModule(module.title)) {
            role.deleteModule(module.title);
        } else {
            role.addModule(module);
        }

        this.roleRepository.save(role, role.id).subscribe(() => {
            this.notificationService.success('Rôle modifié avec succès');
        });
    }

    /**
     * @param {Role} role
     */
    addRole(role: Role) {
        this.roles.push(role);
    }

    /**
     * @param {Role} role
     */
    removeRole(role: Role) {
        if (confirm('Êtes-vous certain de vouloir faire cela ?')) {

            const id = role.id;

            this.roleRepository.remove(id).subscribe(() => {
                this.roles.forEach((item, key) => {
                    if (item.id === id) {
                        this.roles.splice(key, 1);
                    }
                });
                this.notificationService.success('Role supprimé avec succès');
            });
        }
    }

    openDialog(): void {
        this.dialogRef = this.dialog.open(DialodAddRoleComponent, {
            height: '200px',
            width: '300px',
        });
        this.dialogRef.componentInstance['userAccessComponent'] = this;
    }
}
