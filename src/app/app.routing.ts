import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './_guard/auth.guard';
import {DashboardPageComponent} from './pages/dashboard-page/dashboard-page.component';
import {CrmPageComponent} from './pages/crm-page/crm-page.component';
import {BusinessManagementPageComponent} from './pages/business-management-page/business-management-page.component';
import {ProjectMonitoringPageComponent} from './pages/project-monitoring-page/project-monitoring-page.component';
import {IndicatorsPageComponent} from './pages/indicators-page/indicators-page.component';
import {SettingsPageComponent} from './pages/settings-page/settings-page.component';
import {CrmDetailsPageComponent} from './pages/crm-details-page/crm-details-page.component';
import {CrmListPageComponent} from './pages/crm-list-page/crm-list-page.component';
import {CrmContactsPageComponent} from './pages/crm-contacts-page/crm-contacts-page.component';
import {BusinessManagementBusinessListPageComponent} from './pages/business-management-business-list-page/business-management-business-list-page.component';
import {BusinessManagementBusinessDetailsPageComponent} from './pages/business-management-business-details-page/business-management-business-details-page.component';
import {BusinessManagementQuotationListPageComponent} from './pages/business-management-quotation-list-page/business-management-quotation-list-page.component';
import {BusinessManagementQuotationDetailsPageComponent} from './pages/business-management-quotation-details-page/business-management-quotation-details-page.component';
import {AdministationPageComponent} from './pages/administation-page/administation-page.component';
import {UserListPageComponent} from './pages/user-list-page/user-list-page.component';
import {UserDetailsPageComponent} from './pages/user-details-page/user-details-page.component';
import {UserAccessPageComponent} from './pages/user-access-page/user-access-page.component';

const APP_ROUTES: Routes = [
    {
        path: 'login',
        data: {title: 'Connection'},
        component: LoginComponent
    },
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
        canActivate: [AuthGuard]
    },
    {
        path: 'dashboard',
        data: {title: 'Tableau de bord', module: 'dashboard'},
        component: DashboardPageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'crm',
        redirectTo: '/crm/list',
        pathMatch: 'full',
        data: {module: 'crm'},
    },
    {
        path: 'crm',
        data: {module: 'crm'},
        component: CrmPageComponent,
        children: [
            {
                path: 'details',
                children: [
                    {
                        path: '',
                        data: {title: 'CRM - Détails client', module: 'crm'},
                        component: CrmDetailsPageComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: ':id',
                        data: {title: 'CRM - Détails client', module: 'crm'},
                        component: CrmDetailsPageComponent,
                        canActivate: [AuthGuard]
                    },
                ]
            },
            {
                path: 'list',
                data: {title: 'CRM - Liste des clients', module: 'crm'},
                component: CrmListPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'contacts',
                data: {title: 'CRM - Annuaire', module: 'crm'},
                component: CrmContactsPageComponent,
                canActivate: [AuthGuard]
            }
        ]
    },
    {
        path: 'business',
        redirectTo: '/business/list',
        pathMatch: 'full'
    },
    {
        path: 'business',
        component: BusinessManagementPageComponent,
        children: [
            {
                path: 'list',
                data: {title: 'Gestion commerciale - Liste des affaires', module: 'business-management'},
                component: BusinessManagementBusinessListPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'details',
                data: {title: 'Gestion commerciale - Détails affaire', module: 'business-management'},
                component: BusinessManagementBusinessDetailsPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'details/:id',
                data: {title: 'Gestion commerciale - Détails affaire', module: 'business-management'},
                component: BusinessManagementBusinessDetailsPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'quotation/list',
                data: {title: 'Gestion commerciale - Liste des devis', module: 'business-management'},
                component: BusinessManagementQuotationListPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'quotation/details',
                data: {title: 'Gestion commerciale - Détails devis', module: 'business-management'},
                component: BusinessManagementQuotationDetailsPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'quotation/details/:id',
                data: {title: 'Gestion commerciale - Détails devis', module: 'business-management'},
                component: BusinessManagementQuotationDetailsPageComponent,
                canActivate: [AuthGuard]
            },
        ]
    },
    {
        path: 'project-monitoring',
        data: {title: 'Suivi de projet', module: 'project-monitoring'},
        component: ProjectMonitoringPageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'indicators',
        data: {title: 'Indicateurs', module: 'indicators'},
        component: IndicatorsPageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'administration',
        redirectTo: '/administration/user/list',
        pathMatch: 'full',
        data: {module: 'administration'},
    },
    {
        path: 'administration',
        component: AdministationPageComponent,
        children: [
            {
                path: 'user/list',
                data: {module: 'administration', title: 'Administration - Liste des utilisateurs'},
                component: UserListPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'user/details',
                data: {module: 'administration', title: 'Administration - Ajouter un utilisateur'},
                component: UserDetailsPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'user/details/:id',
                data: {module: 'administration', title: 'Administration - Détails utilisateur'},
                component: UserDetailsPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'user/access',
                data: {module: 'administration', title: 'Administration - Droits utilisateurs'},
                component: UserAccessPageComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'settings',
                data: {module: 'administration', title: 'Administration - Préférences'},
                component: SettingsPageComponent,
                canActivate: [AuthGuard]
            },
        ]
    },

    {path: '**', redirectTo: ''}
];

export const Routing = RouterModule.forRoot(APP_ROUTES);
