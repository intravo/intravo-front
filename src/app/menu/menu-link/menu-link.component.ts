import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
    selector: 'app-menu-link',
    templateUrl: 'menu-link.component.html',
    styleUrls: ['menu-link.component.scss']
})
export class MenuLinkComponent implements OnInit {

    @Input() link: string;
    @Input() icon: string;
    @Input() label: string;
    @Input() module: string;

    show: boolean;


    constructor(private authenticationService: AuthenticationService) {
    }

    ngOnInit() {
        this.authenticationService.onLoginChanges().subscribe(auth => {
            this.show = auth.getLoggedInUser().isAllowedToModule(this.module);
        });
    }

}
