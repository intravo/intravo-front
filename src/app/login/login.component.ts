import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';
import {NotificationsService} from '../services/notifications.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    loginForm: FormGroup;
    isLoading: boolean;
    subLogin: Subscription;
    subLoginStateChange: Subscription;

    constructor(private formBuilder: FormBuilder,
                private authenticationService: AuthenticationService,
                private router: Router,
                private notificationService: NotificationsService) {

        this.loginForm = formBuilder.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required]
        });

        this.isLoading = false;
    }

    ngOnInit(): void {
        this.subLogin = this.authenticationService.onLoginChanges().subscribe(() => {
            if (this.authenticationService.isLoggedIn()) {
                this.router.navigate(['dashboard']);
            }
        });

        this.subLoginStateChange = this.authenticationService.onStateLoginChanges().subscribe((loginCode) => {
            this.isLoading = false;

            if (loginCode === AuthenticationService.LOGIN_CODE_SUCCESS) {
                this.notificationService.success('Vous êtes maintenant connecté !');
            } else if (loginCode === AuthenticationService.LOGIN_CODE_BAD_CREDENTIALS) {
                this.notificationService.error('Nom d\'utilisateur ou mot de passe incorrect.');
            } else {
                this.notificationService.error('Une erreur est survenue, veuillez réessayer ultérieurement.');
            }
        });
    }

    ngOnDestroy() {
        this.subLogin.unsubscribe();
        this.subLoginStateChange.unsubscribe();
    }

    onSubmit() {
        this.isLoading = true;
        this.authenticationService.authenticate(
            this.loginForm.value.username,
            this.loginForm.value.password
        );
    }
}
